<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Carrera;
use App\Models\User;


class Noticias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('noticias', function (Blueprint $table) {
            $table->id();
            $table->string('titulo',255)->unique();
            $table->longtext('contenido');
            $table->bigInteger('autor')->unsigned();
            $table->string('imagen',255)->nullable();
            $table->foreign('autor')
            ->references('id')
            ->on('users')
            ->onDelete('cascade');
            $table->bigInteger('carrera_id')->unsigned();
            $table->foreign('carrera_id')
            ->references('id')
            ->on('carreras')
            ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('noticias');
        
    }
}