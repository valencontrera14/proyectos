<?php

namespace Database\Factories;

use App\Models\Noticia_Etiqueta;
use Illuminate\Database\Eloquent\Factories\Factory;

class NoticiaEtiquetaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Noticia_Etiqueta::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
