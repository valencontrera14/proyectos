<?php

namespace Database\Factories;

use App\Models\Carrera;
use Illuminate\Database\Eloquent\Factories\Factory;

class CarreraFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Carrera::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $fecha=$this->faker->dateTimeBetween('-5 years');
        return [
            'nombre'=>$this->faker->word,
            'created_at'=>$fecha,
            'updated_at'=>$this->faker->dateTimeBetween($fecha),
            
        ];
    }
}
