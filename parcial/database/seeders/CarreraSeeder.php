<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Carrera;
use Illuminate\Database\Eloquent\Factories\Factory;
use Database\Factories\CarreraFactory;

class CarreraSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Carrera::factory()->count(50)->create();
    }
}
