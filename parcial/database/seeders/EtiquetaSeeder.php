<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Etiqueta;
use App\Models\User;
use App\Models\Noticia;
use Illuminate\Database\Eloquent\Factories\Factory;
use Database\Factories\EtiquetaFactory;

class EtiquetaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Etiqueta::factory()->count(50)->create();
        Noticia::chunk(2,function($noticias){
            foreach($noticias as $key=> $n)
            {
                $etiquetaRandom=Etiqueta::all()->random(rand(1,3));
                $u=User::all()->random()->id;
            $n->etiquetas()->attach($etiquetaRandom,['user_id'=>$u]/*,['noticia_id'=>$n->id]*/);
            }
        });
    }
}
