@extends('main');

@section('titulo','Noticias')
@section('contenido')

@forelse($contenidoNoticias as $noticia)

@if($loop->iteration % 2!=0)
<div class="row"> 
@endif

    <div class="col col-md-6">
        <div class="card bg-secondary text-center border border-dark m-5 ">
            @if(isset($noticia->imagen))
            @if(Str::startsWith($noticia->imagen, 'http'))
              <img src="{{ $noticia->imagen }}" class="card-img-top" alt="...">
            @else
              <img src="{{ asset('./storage/'. $noticia->imagen) }}" class="card-img-top" alt="...">  
            @endif
         @else
          <h5 class="text-center text-muted"> No hay imagen disponible </h5>
          <hr>
         @endif
            <div class="card-body">
                <h2 class="card-title pb-2 text-dark"><em><b>{{$noticia->titulo}}</b></em></h2>
                    <a href="{{route('carrera',['carrera'=>$noticia->perteneceA->id])}}"><p class="card-text pb-2 text-dark"><small><b><u>{{$noticia->perteneceA->nombre}}</u></b></small></p></a>
                    
                    
            <p class="card-text pb-2 text-white">{!! $noticia->contenido !!}</p>
            <a href="{{route('con-autor',['autor'=>$noticia->creadaPor->id])}}"><p class="card-text pb-2 text-light"><small>By <i>{{$noticia->creadaPor->name}}</i></small></p></a>

            <div class="row">
            <div class="col col-md-12 text-center">
                @foreach($noticia->etiquetas as $e)
                <a href="{{route('etiqueta',['etiqueta'=>$e->id])}}"><span class="badge badge-info text-dark mr-2 mt-2">{{$e->nombre}}</span></a>
                @endforeach
            </div>
            </div>
                
            </div>
            <div class="card-footer text-center">
                <div class="row">
                    <div  class="offset-9 mt-1">
                    <a href="{{route('noticias.show',['noticia'=>$noticia->id])}}" class=" btn btn-danger"><img src="https://www.svgrepo.com/show/38604/eye.svg" 
                    width="20" height="20" alt="Mostrar" title="Mostrar"></a>
</div>
                    <div class="ml-1 mt-1">
                    <a href="{{route('noticias.edit',['noticia'=>$noticia->id])}}" class=" btn btn-danger"><img src="https://www.svgrepo.com/show/6308/pen.svg" 
                    width="20" height="20" alt="Editar" title="Editar"></a>
</div>
                    <div class="ml-1 mt-1">

                    {{Form::model($noticia,['method'=>'delete','route'=>['noticias.destroy',$noticia->id]])}}
                    @csrf
                    
                    <button type="submit" class="btn btn-danger" onclick="if (!confirm('Está seguro de borrar la noticia?')) return false;"><img src="https://www.svgrepo.com/show/80902/trash.svg" width="20" height="20" alt="Borrar" 
                    title="Borrar"></button>
                    {!!Form::close()!!}
                </div>

                </div>
            </div>

        </div>
        </div>

@if($loop->iteration % 2==0)
</div><hr>
@endif

@if($loop->last && $loop->iteration % 2!=0)
</div> <hr>
@endif

@empty 
<div class="alert alert-danger alert-dismissible" role="aler">No hay noticias para mostrar<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>

@endforelse

<div class="d-flex justify-content-center">
{!! $contenidoNoticias->links() !!}
</div>

@endsection