@extends('main');

@section('titulo','Modificar Noticia')
@section('contenido')

<div class="row"> 
    <div class="col col-md-12">
        <div class="card bg-secondary  border border-dark m-5 ">
            <div class="card-header text-center">
              <h1 class="card-title text-dark"><b>Modificar Noticia: "{{$noticia->titulo}}"</b></h1>
            </div>
            <div class="card-body">
              @if(Session::has('status'))
              <div class="alert alert-success">
                {{Session('status')}}
              </div>
              @endif

              {{Form::model($noticia,['method'=>'get','route'=>['noticias.show',$noticia->id]])}}
                @csrf 
              <div class="form-group">
                @error('titulo')
                <div class="alert alert-danger">{{$message}}</div>
                @enderror 
                <em><strong>{{Form::label("Titulo",null,['class'=>'control-label card-text text-dark','for'=>'titulo'])}}</em></strong>
                {{Form::text("titulo",old("titulo"),['class'=>'form-control card-text','readonly'])}}
              </div>

              <div class="form-group">
                @error('contenido')
                <div class="alert alert-danger">{{$message}}</div> 
                @enderror
                <em><strong>{{Form::label("Contenido",null,['class'=>'control-label card-text text-dark','for'=>'contenido'])}}</em></strong>
                {{Form::text("contenido",old("contenido"),['class'=>'form-control card-text','readonly'])}}
              </div>  

              <div class="form-group">
                @error('autor')
                <div class="alert alert-danger">{{$message}}</div> 
                @enderror
                <em><strong>{{Form::label("Autor",null,['class'=>'control-label card-text text-dark','for'=>'autor'])}}</em></strong>
                {{Form::select("autor",$usuario,null,['class'=>'form-control card-text','disabled','readonly'])}}
              </div>

              <div class="form-group">
                @error('carrera_id')
                <div class="alert alert-danger">{{$message}}</div> 
                @enderror
                <em><strong>{{Form::label("Carrera",null,['class'=>'control-label card-text text-dark','for'=>'carrera_id'])}}</em></strong>
                {{Form::select("carrera_id",$carrera,null,['class'=>'form-control card-text','disabled','readonly'])}}
              </div>

              <div class="form-group">
              <em><strong>{{Form::label("Etiquetas",null,['class'=>'control-label card-text text-dark'])}}</em></strong></br>
                @foreach($etiqueta as $id=>$nombre)
                <div class="form-check form-check-inline mt-2 ml-1">
                  <span class="badge badge-info text-dark">
                    @if($noticia->etiquetas()->find($id))
                     {{Form::checkbox("etiqueta".$id,$id,'X',['class'=>'check-input mt-1'])}}
                     {{Form::label($id,$nombre,['class'=>'check-label'])}}
                     @endif
                  </span>
                </div>
                @endforeach
              </div>


              <div class="form-group">
                @error('imagen')
                <div class="alert alert-danger">{{$message}}
                @enderror
                </div> 
                <em><strong>{{Form::label("Imagen",null,['class'=>'control-label card-img text-dark','for'=>'imagen'])}}</em></strong>
                @if($noticia->imagen)
                    @if(Str::startsWith($noticia->imagen,'http'))
                        <img src="{{$noticia->imagen}}" class="img-responsive" width="500" alt="...">
                    @else
                        <img src="{{asset('./storage/'.$noticia->imagen)}}"  width="500" class="img-responsive" alt="...">   
                        @endif
                @else
                    <h5 class="text-center text-muted">No hay imagen para mostrar</h5><hr>
                @endif    
              </div>
              {!!Form::close()!!}

              <div class="card-footer">
              <button class="btn btn-danger " type="submit" style="width:100%;"><strong><a href="{{route('noticias.index')}}" class="text-white">Volver</a></strong></button>
            </div>
              
            
        </div>
    </div>
</div>

@endsection