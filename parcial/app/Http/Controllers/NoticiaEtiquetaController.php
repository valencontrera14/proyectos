<?php

namespace App\Http\Controllers;

use App\Models\Noticia_Etiqueta;
use Illuminate\Http\Request;

class NoticiaEtiquetaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Noticia_Etiqueta  $noticia_Etiqueta
     * @return \Illuminate\Http\Response
     */
    public function show(Noticia_Etiqueta $noticia_Etiqueta)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Noticia_Etiqueta  $noticia_Etiqueta
     * @return \Illuminate\Http\Response
     */
    public function edit(Noticia_Etiqueta $noticia_Etiqueta)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Noticia_Etiqueta  $noticia_Etiqueta
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Noticia_Etiqueta $noticia_Etiqueta)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Noticia_Etiqueta  $noticia_Etiqueta
     * @return \Illuminate\Http\Response
     */
    public function destroy(Noticia_Etiqueta $noticia_Etiqueta)
    {
        //
    }
}
