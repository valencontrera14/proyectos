<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Noticia;
use App\Models\User;
use App\Models\Etiqueta;
use App\Models\Noticia_Etiqueta;
use App\Models\Carrera;


class NoticiaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function mostrarNoticiasSoloImagen($pag=8)
    {
        $contenidoNoticias=Noticia::conImagen()->with('creadaPor')->with('perteneceA')->with('etiquetas')->paginate($pag);
        return view('crearnoticiasimagen',compact('contenidoNoticias'));
    }

    public function porAutor($autorid,$pag=8)
    {
        $autor= User::find($autorid);
        $contenidoNoticias=$autor->noticias()->with('creadaPor')->with('perteneceA')->with('etiquetas')->paginate($pag);
       
        return view('crearnoticiasimagen',compact('contenidoNoticias'));
    }

    public function porCarrera($carrera,$pag=8)
    {
        $e= Carrera::findOrFail($carrera);
        $contenidoNoticias=$e->noticias()->with('creadaPor')->with('perteneceA')->with('etiquetas')->paginate($pag);
       
        return view('crearnoticiasimagen',compact('contenidoNoticias'));
    }

    
    public function porCategoria($categoria,$pag=8)
    {
        $e= Categoria::findOrFail($categoria);
        $contenidoNoticias=$e->noticias()->with('creadaPor')->with('perteneceA')->with('etiquetas')->paginate($pag);
       
        return view('crearnoticiasimagen',compact('contenidoNoticias'));
    }
    
    public function porEtiqueta($etiqueta,$pag=8)
    {
        $e= Etiqueta::findOrFail($etiqueta);
        $contenidoNoticias=$e->noticias()->paginate($pag);
        //dd($etiqueta);
       
        return view('crearnoticiasimagen',compact('contenidoNoticias'));
    }



    public function index()
    {
        $contenidoNoticias=Noticia::with('creadaPor')->with('creadaPor')->with('perteneceA')->with('etiquetas')->paginate(8);
         //dd($contenidoNoticias);
        return view('crearnoticiasimagen',compact('contenidoNoticias'));
    }

    public function create()
    {
        $user=User::pluck('name','id');
        $carrera=Carrera::pluck('nombre','id');
        $etiqueta=Etiqueta::pluck('nombre','id');
        return view('cargarnoticias',compact('user','carrera','etiqueta'));
    }

    
    public function store(Request $request)
    {
        $validatedData=$request->validate(
            [
                'titulo'=>'required|unique:noticias,titulo',
                'contenido'=>'required|max:255',
                'autor'=>'required',
                'carrera_id'=>'required',
                'imagen'=>'max:2048'
            ]
        );

        $n=new Noticia();
        $n->titulo=$request->input('titulo');
        $n->contenido=$request->input('contenido');
        $n->autor=$request->input('autor');
        $n->carrera_id=$request->input('carrera_id');
        $n->save();

        if($request->hasFile('imagen'))
        {
            $imagen=$request->file('imagen');
            $path=$imagen->storeAs('public/noticias/'.$n->id,$imagen->getClientOriginalName());
            $savePath=str_replace("public/","",$path);
            $n->imagen=$savePath;
            $n->save();
        }

        $etiquetas=Etiqueta::all();
        $user=User::all()->random()->id;
        foreach ($etiquetas as $etiqueta)
        {
            if($request->input('etiqueta'.$etiqueta->id))
            {
                $n->etiquetas()->attach($request->input('etiqueta'.$etiqueta->id),['user_id'=>$user]);
            }
        
        }
        $request->session()->flash('status','se guardó correctamente la noticia'.$n->titulo);
        return redirect()->route('noticias.create');
    }

    
    public function show($id)
    {
        $noticia=Noticia::findOrFail($id);
        $usuario=User::pluck('name','id');
        $carrera=Carrera::pluck('nombre','id');
        $etiqueta=Etiqueta::pluck('nombre','id');
        return view('mostrar1noticia',compact('noticia','usuario','carrera','etiqueta'));

    }

   
    public function edit($id)
    {
        $noticia=Noticia::findOrFail($id);
        $usuario=User::pluck('name','id');
        $carrera=Carrera::pluck('nombre','id');
        $etiqueta=Etiqueta::pluck('nombre','id');
        return view('modificarnoticias',compact('noticia','usuario','carrera','etiqueta'));

    }

    
    public function update(Request $request, $id)
    {
        $noticia=Noticia::findOrFail($id);
        $validatedData=$request->validate(
            [
                'titulo'=>'required|unique:noticias,titulo,'.$id,
                'contenido'=>'required|max:255',
                'autor'=>'required',
                'carrera_id'=>'required',
                'imagen'=>'max:2048'
            ]
        );

        if($request->hasFile('imagen'))
        {
            $imagen=$request->file('imagen');
            $path=$imagen->storeAs('public/noticias/'.$noticia->id,$imagen->getClientOriginalName());
            $savePath=str_replace("public/","",$path);
            $noticia->imagen=$savePath;
            $noticia->save();
        }

        $noticia->update($validatedData);
        $noticia->autor=$request->input('autor');
        $noticia->carrera_id=$request->input('carrera_id');
        $noticia->save();

        $etiquetas=Etiqueta::all();
        $user=User::all()->random()->id;
        $noticia->save();

        $noticia->etiquetas()->detach();
        foreach ($etiquetas as $etiqueta)
        {
            if($request->input('etiqueta'.$etiqueta->id))
            {
                $noticia->etiquetas()->attach($request->input('etiqueta'.$etiqueta->id),['user_id'=>$user]);
            }
        
        }

        $request->session()->flash('status','Se modificó correctamente la noticia: '.$noticia->titulo);
        return redirect()->route('noticias.edit',$noticia->id);
    }

    
    public function destroy($id)
    {
        $noticia=Noticia::findOrFail($id);
        $noticia->etiquetas()->detach();
        $noticia->delete();
        return redirect()->route('noticias.index');

    }
}
