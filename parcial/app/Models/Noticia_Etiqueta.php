<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\Pivot;
use App\Models\User;
use App\Models\Noticia;
use App\Models\Etiqueta;

class Noticia_Etiqueta extends Pivot
{
    use SoftDeletes;
    use HasFactory;
    protected $dates=['delete_at'];
    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
}
