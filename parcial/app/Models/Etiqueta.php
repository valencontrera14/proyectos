<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\User;
use App\Models\Noticia;
use App\Models\Noticia_Etiqueta;
use Illuminate\Database\Eloquent\Relations\Pivot;

class Etiqueta extends Model
{
    use SoftDeletes;
    use HasFactory;
    protected $dates=['delete_at'];

    public function noticias()
    {
        return $this->belongsToMany(Noticia::class,'noticias_etiquetas')
        ->withPivot('user_id')
        ->withTimestamps()
        ->using(Noticia_Etiqueta::class);
    }
}
