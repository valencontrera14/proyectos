<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Noticia;

class Carrera extends Model
{
    use SoftDeletes;
    use HasFactory;
    protected $dates=['delete_at'];
    public function noticias()
    {
        return $this->hasMany(Noticia::class,'carrera_id');
    }
}
