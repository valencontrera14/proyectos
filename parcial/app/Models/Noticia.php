<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\User;
use App\Models\Carrera;
use App\Models\Etiqueta;
use App\Models\Noticia_Etiqueta;
use Illuminate\Database\Eloquent\Relations\Pivot;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Noticia extends Model
{
    
    use SoftDeletes;
    use HasFactory;
    protected $dates=['delete_at'];
    protected $fillable=['titulo','contenido','autor','carrera_id'];

    static public function conImagen()
    {
        return Noticia::whereNotNull('imagen');
    }

     public function creadaPor()
    {
        return $this->belongsTo(User::class,'autor');
    }
    public function perteneceA()
    {
        return $this->belongsTo(Carrera::class,'carrera_id');
    }
    public function etiquetas()
    {
        return Noticia::belongsToMany(Etiqueta::class,'noticias_etiquetas')
        ->withPivot('user_id')
        ->withTimestamps()
        ->using(Noticia_Etiqueta::class);
    }

    
}
