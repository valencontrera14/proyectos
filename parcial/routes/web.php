<?php

use Illuminate\Support\Facades\Route;
use App\Models\Noticia;
use App\Http\Controllers\NoticiaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::resource('noticias',NoticiaController::class);
Route::get('noticiasSoloImagen/{pag?}',[NoticiaController::class, 'mostrarNoticiasSoloImagen'])->name('solo-imagen');
Route::get('noticiasAutor/{autor}/{pag?}',[NoticiaController::class, 'porAutor'])->name('con-autor');
Route::get('noticiasCarrera/{carrera}/{pag?}',[NoticiaController::class, 'porCarrera'])->name('carrera');
Route::get('noticiasCategoria/{categoria}/{pag?}',[NoticiaController::class, 'porCategoria'])->name('categoria');
Route::get('noticiasEtiqueta/{etiqueta}/{pag?}',[NoticiaController::class, 'porEtiqueta'])->name('etiqueta');
